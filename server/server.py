import SocketServer
from host_info import Host
import json
from sys import argv

hosts = {}

json = {"address":"","port":"","methods":{"name":"desc","name":"desc"}}

class RPCMappingHandler(SocketServer.BaseRequestHandler):

	def handle(self):
		data = json.loads(self.request.recv(2048).strip())
		excluded = None
		if "address" in data and "port" in data:
			excluded = (data["address"],data["port"])
			if "methods" in data and len(data["methods"]) > 0:
				hosts[(data["address"],data["port"])] = Host(data["address"],data["port"],data["methods"])	
		self.request.send(hostsToJSON(excluded))

def hostsToJSON(excluded):
	local_hosts = []
	for key in hosts:
		if key != excluded:
			local_hosts.append(hosts[key].toMap())
	return json.dumps({"hosts":local_hosts})

HOST = "127.0.0.1"
PORT = argv[1]

server = SocketServer.TCPServer((HOST,PORT),RPCMappingHandler)
server.serve_forever()
